#ifndef _SORTMULTINOMIALLAYERORDEREDHEAPGENERATOR_HPP
#define _SORTMULTINOMIALLAYERORDEREDHEAPGENERATOR_HPP

#include "MultinomialLayerOrderedHeapGenerator.hpp"

template <typename T1, typename T2>
class pair_compare_only_first_axis : public std::pair<T1,T2> {
public:
  bool operator <(const pair_compare_only_first_axis<T1,T2> & rhs) const {
    return this->first < rhs.first;
  }
};

template <typename T1, typename T2>
pair_compare_only_first_axis<T1,T2> make_pair_compare_only_first_axis(const T1 & a1, const T2 & a2) {
  return pair_compare_only_first_axis<T1,T2>{{a1,a2}};
}

template <unsigned NUM_ISOTOPES>
class SortMultinomialLayerOrderedHeapGeneratorFixedSize : public MultinomialLayerOrderedHeapGeneratorFixedSize<NUM_ISOTOPES> {
protected:
  struct IndexTupleCandidate {
    IndexTuple<NUM_ISOTOPES> tup;
    int max_i_advanced, max_j_advanced;
  };

  // Note: using PrimitiveVector is safe in spite of warnings;
  // IndexTuple constructor is not needed here as long as calloc is used.
  //PrimitiveVector<std::pair<double, IndexTuple<NUM_ISOTOPES> >, 65536ul> _hull_heap; // start with space reserved
  std::vector<pair_compare_only_first_axis<double, IndexTupleCandidate> > _hull_heap;

  void _insert_neighbors_in_bounds(const IndexTupleCandidate & current) {
    const IndexTuple<NUM_ISOTOPES> & tup = current.tup;
    IndexTupleCandidate candidate = current;

    // To avoid needing a hull set, only advance axes when no subsequent axes have advanced;
    // this prevents (+1,0) and (0,+1) from both yielding neighbor (+1,+1).
    // All neighbors are still reachable in the same number of L1 jumps. Because L1 distance
    // from the mode implies lower probability, all neighbors are still considered in the same order.
    for (unsigned i=current.max_i_advanced; i<NUM_ISOTOPES; ++i)
      for (unsigned j=current.max_j_advanced; j<NUM_ISOTOPES; ++j)
	if (i != j) {
	  if (tup[j] >= 1) {
            // ensure we get consistently further from the mode; this
            // prevents tuples from being added again without needing
            // to store a set of every tuple visited.
	    if (tup[i]>=this->_mode[i] && tup[j]<=this->_mode[j]) {

	      candidate.tup.increment(i);
	      candidate.tup.decrement(j);
	      
	      candidate.max_i_advanced = i;
	      candidate.max_j_advanced = j;
	      
	      _insert_tuple(candidate);
	      
	      candidate.tup.decrement(i);
	      candidate.tup.increment(j);
	    }
	  }
	}
  }

  void _insert_tuple(const IndexTupleCandidate & candidate) {
    double log_prob = this->_log_multinomial_probability(candidate.tup);
    _hull_heap.push_back( make_pair_compare_only_first_axis(log_prob, candidate) );
    std::push_heap(_hull_heap.begin(), _hull_heap.end());
  }

  LogAbundanceAndMass get_next_value() {
    #ifdef SAFE
    assert(_hull_heap.size()>0);
    #endif

    std::pop_heap(_hull_heap.begin(), _hull_heap.end());

    pair_compare_only_first_axis<double, IndexTupleCandidate> most_recent_result = _hull_heap.back();
    _hull_heap.pop_back();

    _insert_neighbors_in_bounds(most_recent_result.second);

    return LogAbundanceAndMass{most_recent_result.first, this->_compute_mass(most_recent_result.second.tup)};
  }

public:
  SortMultinomialLayerOrderedHeapGeneratorFixedSize(LayerArithmetic*la, const double*log_probabilities, unsigned power, const double*masses, const LogFactorialCache & log_factorial_cache):
    MultinomialLayerOrderedHeapGeneratorFixedSize<NUM_ISOTOPES>(la, log_probabilities, power, masses, log_factorial_cache)//,
  {
    // Pretend mode has been advanced along i=0 and j=0; these are the
    // least constraining and will have no effect.
    _insert_tuple(IndexTupleCandidate{this->_mode, 0,0});
  }

  void compute_next_layer_if_available() override {
    if (this->are_more_layers_available()) {
      unsigned long new_layer_size = this->layer_size(this->_number_of_layers_generated);
      LogAbundanceAndMass*new_layer = (LogAbundanceAndMass*)malloc(new_layer_size*sizeof(LogAbundanceAndMass));
      for (unsigned long i=0; i<new_layer_size; ++i)
	new_layer[i] = get_next_value();
      this->add_layer(new_layer, new_layer_size);
    }
  }
};

#endif
