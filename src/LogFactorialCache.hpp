#ifndef _LOGFACTORIALCACHE_HPP
#define _LOGFACTORIALCACHE_HPP

#include <math.h>

class LogFactorialCache {
private:
  double*_index_to_log_factorial;
  long _max_index;
public:
  LogFactorialCache(unsigned long max_index) {
    _max_index = max_index;
    _index_to_log_factorial = new double[max_index+1];

    for (unsigned long i=0; i<=max_index; ++i)
      _index_to_log_factorial[i] = lgamma(i+1);
  }

  ~LogFactorialCache(){
    delete[] _index_to_log_factorial;
  }

  double value(int i) const {
    #ifdef SAFE
    assert(i < _max_index && i >= 0);
    #endif
    return _index_to_log_factorial[i];
  }
};

#endif
