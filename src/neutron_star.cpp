#include "io.hpp"
#include "IsotopeCalculator.hpp"
#include "LayerOrderedHeap.hpp"
#include "Clock.hpp"

#include <unordered_map>
#include <string.h>

// Placed first so it can be used when errors are encountered during parsing:
void print_usage_error() {
  printn("Usage: <molecule (e.g. water is H2O1)> <-k or -p>  <k or p> <alpha> [merge_identical_masses] [sort_by_abundance]");
  exit(1);
}

void merge_identical_masses(LogAbundanceAndMass*result, unsigned long & k) {
  // Note: sorting might be faster in practice:
  std::unordered_map<double, double> mass_to_log_abundance;

  for (unsigned long i=0; i<k; ++i) {
    double m = result[i].mass;
    double log_prob = result[i].log_abundance;
    if (mass_to_log_abundance.find(m) == mass_to_log_abundance.end())
      mass_to_log_abundance[m] = log_prob;
    else
      mass_to_log_abundance[m] = log_add(mass_to_log_abundance[m], log_prob);
  }
    
  k = mass_to_log_abundance.size();
  unsigned long i=0;
  for (const std::pair<double, double> & p : mass_to_log_abundance) {
    result[i].mass = p.first;
    result[i].log_abundance = p.second;
    ++i;
  }
}

int main(int argc, char**argv) {
  std::cout.precision(100);

  bool merge_identical_masses_flag=false;
  bool sort_by_abundance_flag=false;

  if (argc == 5) {
    // vanilla
  }
  else if (argc == 6) {
    // one optional arg
    std::string arg(argv[5]);
    if (arg == "merge_identical_masses")
      merge_identical_masses_flag = true;
    else if (arg == "sort_by_abundance")
      sort_by_abundance_flag = true;
    else
      print_usage_error();
  }
  else if (argc == 7) {
    // two optional args
    std::string arg_a(argv[5]);
    std::string arg_b(argv[6]);
    
    merge_identical_masses_flag = true;
    sort_by_abundance_flag = true;
    
    if (arg_a == "merge_identical_masses" && arg_b == "sort_by_abundance") {}
    else if (arg_b == "merge_identical_masses" && arg_a == "sort_by_abundance") {}
    else
      print_usage_error();
  }
  else
    print_usage_error();

  std::string molecule(argv[1]);
  std::string k_or_p(argv[2]);
  
  double alpha = read<double>(argv[4]);
  LayerArithmetic *la;

  unsigned long k;
  double p=0;
  bool run_with_k = false;

  if (k_or_p.compare("-k") == 0) {
    k = read<unsigned long>(argv[3]);
    // Prefetch some layer sizes and indices for greater performance:
    unsigned long n_guaranteed = ceil(alpha)*ceil(alpha)*k;
    la = new LayerArithmetic(alpha, n_guaranteed);
    run_with_k = true;
  } 
  else if (k_or_p.compare("-p") == 0) {
    p = read<double>(argv[3]);
    // Prefetch some layer sizes and indices for greater performance:
    unsigned long n_guaranteed = 10000;
    la = new LayerArithmetic(alpha, n_guaranteed);
  }
  else 
    print_usage_error();

  LogAbundanceAndMass* result;
  Clock c;
  IsotopeCalculator calculator(molecule, la);
  if (run_with_k)
    result = calculator.select(k);
  else {
    std::pair<LogAbundanceAndMass*, unsigned long> result_and_k = calculator.select_by_p(p);
    result = result_and_k.first;
    k = result_and_k.second;
  }

  c.ptock();

  if (merge_identical_masses_flag) {
    merge_identical_masses(result, k);
    // Layer-ordered heapify result:
    if (!sort_by_abundance_flag)
      LayerOrderedHeap<LogAbundanceAndMass> loh(result, k, la);
  }

  if (sort_by_abundance_flag)
    std::sort(result, result+k, [](LogAbundanceAndMass a, LogAbundanceAndMass b){return a.log_abundance > b.log_abundance;});

  printf("%-10s  %-30s  %-30s  %-30s\n", "RANK", "LOG ABUNDANCE", "ABUNDANCE", "MASS");
  double log_total_prob = -1.0/0.0;
  for (unsigned long i=0; i<k; ++i) {
    double log_prob = result[i].log_abundance;
    double prob = exp(log_prob);
    printf("%-10lu  %-30.21g  %-30.21g  %-30.21g\n", i, log_prob, prob, result[i].mass);
    log_total_prob = log_add(log_total_prob, log_prob);
  }

  log_total_prob = std::min(log_total_prob,0.0);
  printf("TOTAL LOG ABUNDANCE %.21g\n", log_total_prob);
  printf("TOTAL ABUNDANCE %.21g\n", exp(log_total_prob));

  delete[] result;
  delete la;
  return 0;
}
