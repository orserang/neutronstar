#ifndef _CHANGETUPLEITERATOR_HPP
#define _CHANGETUPLEITERATOR_HPP

#include "TemplateSearch.hpp"

// See TRIOT paper, Heyl & Serang

template <unsigned TUPLE_LENGTH, unsigned TUPLE_INDEX>
struct ChangeTupleIteratorHelper {
  template <typename FUNCTION>
  static void apply(int*__restrict change_tup, const int*__restrict change_from_where_tup, bool any_nonzero, FUNCTION function) {
    // Once advanced from the origin, only allowed to advance in the
    // same direction (or else the same tuple could be produced twice)
    int min_bound = -1;
    if (change_from_where_tup[TUPLE_INDEX] > 0)
      min_bound = 0;
    int max_bound = 1;
    if (change_from_where_tup[TUPLE_INDEX] < 0)
      max_bound = 0;
    for (int i=min_bound; i<=max_bound; ++i) {
      change_tup[TUPLE_INDEX] = i;

      ChangeTupleIteratorHelper<TUPLE_LENGTH, TUPLE_INDEX-1>::apply(change_tup, change_from_where_tup, any_nonzero || (i!=0), function);
    }
  }
};

template <unsigned TUPLE_LENGTH>
struct ChangeTupleIteratorHelper<TUPLE_LENGTH, 0u> {
  template <typename FUNCTION>
  static void apply(int*__restrict change_tup, const int*__restrict change_from_where_tup, bool any_nonzero, FUNCTION function) {
    int min_bound = -1;
    if (change_from_where_tup[0] > 0)
      min_bound = 0;
    int max_bound = 1;
    if (change_from_where_tup[0] < 0)
      max_bound = 0;
    for (int i=min_bound; i<=max_bound; ++i) {
      change_tup[0] = i;

      // Do not allow change of [0,0,0,...]
      if (any_nonzero || i != 0)
	function(change_tup, TUPLE_LENGTH);
    }
  }
};

template <unsigned TUPLE_LENGTH>
struct ChangeTupleIterator {
  template <typename FUNCTION>
  static void apply(int*__restrict change_tup, const int*__restrict change_from_where_tup, FUNCTION function) {
    ChangeTupleIteratorHelper<TUPLE_LENGTH, TUPLE_LENGTH-1>::apply(change_tup, change_from_where_tup, false, function);
  }
};

#endif
