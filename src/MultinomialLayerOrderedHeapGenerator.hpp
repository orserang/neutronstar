#ifndef _MULTINOMIALLAYERORDEREDHEAPGENERATOR_HPP
#define _MULTINOMIALLAYERORDEREDHEAPGENERATOR_HPP

#include "LogAbundanceAndMass.hpp"
#include "element_to_log_abundance_and_masses.hpp"
#include "DynamicLayerOrderedHeapGenerator.hpp"
#include "IndexTuple.hpp"
#include "PrimitiveVector.hpp"
#include "FixedSumTupleIterator.hpp"
#include "ChangeTupleIterator.hpp"
#include "LogFactorialCache.hpp"

#include <vector>
#include <unordered_set>
#include <algorithm>

class MultinomialLayerOrderedHeapGenerator : public DynamicLayerOrderedHeapGenerator<LogAbundanceAndMass> {
protected:
  const double*_log_probabilities;
  unsigned _power;
  const double*_masses;
  const LogFactorialCache & _log_factorial_cache;

  double _log_binomial_coefficient(int n, int x) const {
    return _log_factorial_cache.value(n) - _log_factorial_cache.value(x) - _log_factorial_cache.value(n-x);
  }

  double _log_binomial_probability(unsigned n, double p, unsigned x) const {
    return _log_binomial_coefficient(n,x) + x*log(p) + (n-x)*log(1-p);
  }

  unsigned _binomial_mode(unsigned n, double p) const {
    unsigned m1 = floor( (n+1)*p );
    unsigned m2 = m1+1;

    if (m1 == n)
      return m1;

    double x1 = _log_binomial_probability(n, p, m1);
    double x2 = _log_binomial_probability(n, p, m2);
    if (x1 > x2)
      return m1;
    return m2;
  }

public:
  MultinomialLayerOrderedHeapGenerator(LayerArithmetic*la, const double*log_probabilities, unsigned power, const double*masses, const LogFactorialCache & log_factorial_cache):
    DynamicLayerOrderedHeapGenerator(la),
    _log_probabilities(log_probabilities),
    _power(power),
    _masses(masses),
    _log_factorial_cache(log_factorial_cache)
  { }
};

template <unsigned N>
class MultinomialLayerOrderedHeapGeneratorFixedSize : public MultinomialLayerOrderedHeapGenerator {
protected:
  IndexTuple<N> _mode;

  double _log_multinomial_probability(const int*tup) const {
    double result = _log_factorial_cache.value(_power);
    for (unsigned i=0; i<N; ++i) {
      int t = tup[i];
      result -= _log_factorial_cache.value(t);
      result += t*_log_probabilities[i];
    }
    return result;
  }

  double _log_multinomial_probability_change(const int*old_tup, unsigned i, unsigned j, int change) const {
    // old_tup[i]! --> (old_tup[i]+change)!
    // old_tup[j]! --> (old_tup[i]-change)!
    // probabilities[i]^old_tup[i] --> probabilities[i]^(old_tup[i]+change)
    // probabilities[j]^old_tup[j] --> probabilities[j]^(old_tup[j]-change)
    
    // Note: difference here is a log; could use another lookup table?
    return _log_factorial_cache.value(old_tup[i]) - _log_factorial_cache.value(old_tup[i]+change) + _log_factorial_cache.value(old_tup[j]) - _log_factorial_cache.value(old_tup[j]-change) + change*_log_probabilities[i] - change*_log_probabilities[j];
  }

  void _initialize_mode() {
    long initial_sum = 0;
    if (N > 1){
      for (unsigned i=0; i<N; ++i) {
        _mode.set(i, _binomial_mode(_power, exp(_log_probabilities[i])));
        initial_sum += _mode[i];
      }
      if (initial_sum != _power) {
	long excess = initial_sum - _power;
	if (excess < 0)
	  _mode.set(0, _mode[0] - excess);
	else {
	  // may not be able to subtract enough from first class:
	  for (unsigned i=0; i<N && excess > 0; ++i) {
	    long max_subtractable = _mode[i];
	    long subtract_amount = std::min(max_subtractable, excess);
	    _mode.set(i, _mode[i] - subtract_amount);
	    excess -= subtract_amount;
	  }
	}
      }
    }else
      _mode.set(0, _power);

    // Locally optimize until mode is reached:
    double mode_quality = _log_multinomial_probability(_mode);

    IndexTuple<N> candidate;
    bool any_changes_accepted;
    do {
      candidate = _mode;
      any_changes_accepted = false;
      for (unsigned i=0; i<N; ++i) 
	for (unsigned j=i+1; j<N; ++j) {
	  for (int change : {-1,1}) {
	    candidate.set(i, candidate[i]+change);
	    candidate.set(j, candidate[j]-change);

	    if (candidate[i] >= 0 && candidate[j] >= 0) {
	      double candidate_quality = _log_multinomial_probability(candidate);

	      if (candidate_quality > mode_quality) {
		_mode = candidate;
		mode_quality = candidate_quality;
		any_changes_accepted = true;
	      }
	    }
	    // change back:
	    candidate.set(i, candidate[i]-change);
	    candidate.set(j, candidate[j]+change);
	  }
	}
    } while (any_changes_accepted);
  }

  double _compute_mass(const int*tup) const {
    double mass = 0.0;
    for (unsigned i=0; i<N; ++i)
      mass += tup[i]*_masses[i];
    return mass;
  }

public:
  MultinomialLayerOrderedHeapGeneratorFixedSize(LayerArithmetic*la, const double*log_probabilities, unsigned power, const double*masses, const LogFactorialCache & log_factorial_cache):
    MultinomialLayerOrderedHeapGenerator(la, log_probabilities, power, masses, log_factorial_cache)
  {
    _initialize_mode();
    this->_number_of_elements_possible = round(exp(this->_log_binomial_coefficient(N+this->_power-1, this->_power)));
  }
};

#endif
