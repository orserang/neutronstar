#ifndef _INDEXTUPLE_HPP
#define _INDEXTUPLE_HPP

#include "io.hpp"

#include <array>
#include <stdio.h>

//#define CACHE_HASH

// int should be sufficient for very large problems here; because of
// multinomial coefficient, an axis growing to 2^31-1 would mean it's
// a very large problem:
template <unsigned SIZE>
class IndexTuple {
private:
  std::array<int, SIZE> _data;
  #ifdef CACHE_HASH
  unsigned long _cached_hash;
  #endif

public:
  static constexpr int SHIFT = (sizeof(unsigned long)*7 / SIZE);

  IndexTuple() {
    for (unsigned i=0; i<SIZE; ++i)
      _data[i] = 0;
    #ifdef CACHE_HASH
    _cached_hash = 0;
    #endif
  }

  #ifdef CACHE_HASH
  unsigned long cached_hash() const {
    return _cached_hash;
  }
  #endif

  void set(unsigned i, int val) {
    // Remove contribution to hash from old element value:
     #ifdef CACHE_HASH
    _cached_hash ^= ((*this)[i] << (i*SHIFT));
    #endif

    // Change element value:
    _data[i] = val;

    // Add contribution to hash from new element value:
     #ifdef CACHE_HASH
    _cached_hash ^= (val << (i*SHIFT));
    #endif
  }
  int operator[](unsigned i) const {
    return _data[i];
  }
  void increment(unsigned i, long change=1) {
    set(i, _data[i]+change);
  }
  void decrement(unsigned i, long change=1) {
    set(i, _data[i]-change);
  }

  operator const int*() const {
    return &_data[0];
  }

  auto begin() const -> decltype(_data.begin()) {
    return _data.begin();
  }
  auto end() const -> decltype(_data.end()) {
    return _data.end();
  }
};

template <unsigned SIZE>
std::ostream & operator <<(std::ostream & os, const IndexTuple<SIZE> & tup) {
  os << "tup[ ";
  std::operator<<(os, str(tup.begin(), tup.end()));
  os << "]";
  return os;
}

template <unsigned SIZE>
bool operator==(const IndexTuple<SIZE> & lhs, const IndexTuple<SIZE> & rhs) {
  #ifdef CACHED_HASH
  if (lhs.cached_hash() != rhs.cached_hash())
    return false;
  #endif
  for (unsigned i=0; i<SIZE; ++i)
    if (lhs[i] != rhs[i])
      return false;
  return true;
}

template <unsigned SIZE>
bool operator<(const IndexTuple<SIZE> & lhs, const IndexTuple<SIZE> & rhs) {
  for (unsigned i=0; i<SIZE; ++i)
    if (lhs[i] < rhs[i])
      return true;
    else if (lhs[i] > rhs[i])
      return false;

  // equal
  return false;
}

template <unsigned SIZE>
bool operator>(const IndexTuple<SIZE> & lhs, const IndexTuple<SIZE> & rhs) {
  return !(lhs < rhs || lhs == rhs);
}

struct IndexTupleHash {
  template <unsigned SIZE>
  std::size_t operator() (const IndexTuple<SIZE> & it) const {
    #ifdef CACHE_HASH
    return it.cached_hash();
    #else
    std::size_t combined_hash_value = 0;

    for (int i : it) {
      // Shift to fit each axis inside an unsigned long. XOR oerlapping bits. 
      combined_hash_value <<= IndexTuple<SIZE>::SHIFT;
      combined_hash_value ^= i;
    }

    return combined_hash_value;
    #endif
  }
};

#endif
