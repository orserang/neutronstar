#ifndef _BIRTHDAYMULTINOMIALLAYERORDEREDHEAPGENERATOR_HPP
#define _BIRTHDAYMULTINOMIALLAYERORDEREDHEAPGENERATOR_HPP

#include "MultinomialLayerOrderedHeapGenerator.hpp"
#include "FixedSumTupleIterator.hpp"
#include "ChangeTupleIterator.hpp"

template <unsigned NUM_ISOTOPES>
struct LayerProductIndex {
  // The index of each layer. The cumulative layer sizes correspond to shifts from the mode index.
  IndexTuple<NUM_ISOTOPES> layer_indices;
  bool is_max;

  // Only set when is_max = true
  unsigned long num_values_contained;
};

struct LayerProductIndexTupleHash {
  template <unsigned NUM_ISOTOPES>
  std::size_t operator() (const LayerProductIndex<NUM_ISOTOPES> & lpi) const {
    IndexTupleHash single_hash;
    return single_hash(lpi.layer_indices) ^ lpi.is_max;
  }
};

template <unsigned NUM_ISOTOPES>
std::ostream & operator <<(std::ostream & os, const LayerProductIndex<NUM_ISOTOPES> & lpi) {
  os << "LayerProductIndex[" << lpi.layer_indices << ", " << lpi.is_max;
    if (lpi.is_max)
    os << ", " << lpi.num_values_contained;
  os << "]";
  return os;
}

template <unsigned NUM_ISOTOPES>
bool operator<(const LayerProductIndex<NUM_ISOTOPES> & lhs, const LayerProductIndex<NUM_ISOTOPES> & rhs) {
  if (lhs.layer_indices < rhs.layer_indices)
    return true;
  if (lhs.layer_indices > rhs.layer_indices)
    return false;

  return lhs.is_max < rhs.is_max;
}

template <unsigned NUM_ISOTOPES>
bool operator==(const LayerProductIndex<NUM_ISOTOPES> & lhs, const LayerProductIndex<NUM_ISOTOPES> & rhs) {
  if (lhs.layer_indices == rhs.layer_indices && lhs.is_max == rhs.is_max)
    return true;
  return false;
}

template <unsigned NUM_ISOTOPES>
class BirthdayMultinomialLayerOrderedHeapGeneratorFixedSize : public MultinomialLayerOrderedHeapGeneratorFixedSize<NUM_ISOTOPES> {
protected:
  unsigned long _num_max_corner_elements_popped;
  unsigned long _all_cumulative_selections_to_date;
  
  PrimitiveVector<LogAbundanceAndMass> _values_considered;

  // Uses log probability as .first:
  // Try PrimitiveVector:
  std::vector<std::pair<double, LayerProductIndex<NUM_ISOTOPES> > > _hull_heap;
  std::unordered_set<LayerProductIndex<NUM_ISOTOPES>, LayerProductIndexTupleHash> _hull_set;

  bool _in_bounds(const IndexTuple<NUM_ISOTOPES> & element_index) const {
    for (unsigned long i=0; i<NUM_ISOTOPES; ++i)
      if (element_index[i]<0 || element_index[i]>int(this->_power))
	return false;
    return true;
  }

  bool _any_in_tile_that_sum_to_power(const IndexTuple<NUM_ISOTOPES> & element_start_corner, const IndexTuple<NUM_ISOTOPES> & element_inclusive_end_corner) const {
    // Check tile to see if number of isotopes equals total number of
    // the elementin the isotopologue
    int smaller_sum = 0, larger_sum = 0;
    for (unsigned i=0; i<NUM_ISOTOPES; ++i) {
      if (element_start_corner[i] <= element_inclusive_end_corner[i]) {
	smaller_sum += element_start_corner[i];
	larger_sum += element_inclusive_end_corner[i];
      }
      else {
	smaller_sum += element_inclusive_end_corner[i];
	larger_sum += element_start_corner[i];
      }
    }
    
    return int(this->_power) >= smaller_sum && int(this->_power) <= larger_sum;
  }

  void _insert_neighbors_in_bounds(const IndexTuple<NUM_ISOTOPES> & tup) {
    IndexTuple<NUM_ISOTOPES> new_tup;
    int change_tup[NUM_ISOTOPES];

    auto lambda = [this, &tup, &new_tup](int*change, unsigned dimension) {
      for (unsigned i=0; i<dimension; ++i)
	new_tup.set(i, tup[i] + change[i]);

      IndexTuple<NUM_ISOTOPES> element_min_corner = _min_corner_index(new_tup);
      IndexTuple<NUM_ISOTOPES> element_max_corner = _inclusive_max_corner_index(new_tup);

      if (_in_bounds(element_min_corner) && _any_in_tile_that_sum_to_power(element_min_corner, element_max_corner)) {
	LayerProductIndex<NUM_ISOTOPES> min_corner{new_tup,false};
	_insert_layer_product(min_corner);
      }
    };
    ChangeTupleIterator<NUM_ISOTOPES>::apply(change_tup, tup, lambda);
  }

  void _insert_layer_product(const LayerProductIndex<NUM_ISOTOPES> & index) {
    if (_hull_set.find(index) == _hull_set.end()) {
      _hull_set.insert(index);
      _hull_heap.push_back(_create_layer_product(index));

      // Ignore IndexTuple comparison in pair (multinomial cannot be flat, so there is no need to break ties):
      std::push_heap(_hull_heap.begin(), _hull_heap.end(), [](auto lhs, auto rhs){return lhs.first < rhs.first;});
    }
  }

  LogAbundanceAndMass*_select(unsigned long k) {
    // Note: k here is the new selection amount (not the cumulative)
    _all_cumulative_selections_to_date += k;
    #ifdef SAFE
    assert( _all_cumulative_selections_to_date <= this->n_elements_possible() );
    #endif
    while (_num_max_corner_elements_popped < _all_cumulative_selections_to_date)
      _pop_next_layer_product();

    #ifdef SAFE
    assert(_values_considered.size() >= k);
    #endif
    unsigned long num_unused_vals = _values_considered.size() - k;
    std::nth_element(_values_considered.begin(), _values_considered.begin()+num_unused_vals-1, _values_considered.end(), [](auto lhs, auto rhs){return lhs>rhs;});

    LogAbundanceAndMass*result = new LogAbundanceAndMass[k];
    std::copy(_values_considered.begin()+num_unused_vals, _values_considered.end(), result);
    _values_considered.resize(num_unused_vals);
    return result;
  }
  
  IndexTuple<NUM_ISOTOPES> _inclusive_max_corner_index(const IndexTuple<NUM_ISOTOPES> & layer_indices) const {
    IndexTuple<NUM_ISOTOPES> result = this->_mode;

    // ...end_index is not inclusive (as iterators are), so do - 1 so that the result is inclusive
    for (unsigned i=0; i<NUM_ISOTOPES; ++i) {
      long layer_shift = layer_indices[i];
      if (layer_shift >= 0)
	result.set(i, result[i] + this->_la->get_flat_layer_end_index(layer_shift)-1);
      else
	result.set(i, result[i] - (this->_la->get_flat_layer_end_index(-layer_shift)-1));
      }
    return result;
  }

  IndexTuple<NUM_ISOTOPES> _min_corner_index(const IndexTuple<NUM_ISOTOPES> & layer_indices) const {
    IndexTuple<NUM_ISOTOPES> result = this->_mode;
    for (unsigned i=0; i<NUM_ISOTOPES; ++i) {
      long layer_shift = layer_indices[i];
      if (layer_shift >= 0)
	result.set(i, result[i] + this->_la->get_flat_layer_start_index(layer_shift));
      else
	result.set(i, result[i] - this->_la->get_flat_layer_start_index(-layer_shift));
    }
    return result;
  }

  IndexTuple<NUM_ISOTOPES> _inclusive_corner_index(const LayerProductIndex<NUM_ISOTOPES> & index) const {
    if (index.is_max)
      return _inclusive_max_corner_index(index.layer_indices);
    return _min_corner_index(index.layer_indices);
  }

  IndexTuple<NUM_ISOTOPES> _most_probable_index_in_layer_product(const LayerProductIndex<NUM_ISOTOPES> & index) const {
    IndexTuple<NUM_ISOTOPES> min_corner = _min_corner_index(index.layer_indices);
    IndexTuple<NUM_ISOTOPES> max_corner_inclusive = _inclusive_max_corner_index(index.layer_indices);
    int fixed_sum_tuple_iter[NUM_ISOTOPES];
    IndexTuple<NUM_ISOTOPES> result;
    long l1_dist = std::numeric_limits<long>::max();

    auto lambda = [this, &result, &l1_dist](int*fixed_sum_tuple, unsigned dimension) {
      long tmp_l1_dist = 0;
      for (unsigned long i=0; i<dimension; ++i)
	tmp_l1_dist += abs(this->_mode[i] - fixed_sum_tuple[i]);
      
      if (tmp_l1_dist < l1_dist) {
	l1_dist = tmp_l1_dist;
	for (unsigned long i=0; i<dimension; ++i)
	  result.set(i, fixed_sum_tuple[i]);
      }
    };
    
    IndexTuple<NUM_ISOTOPES> lower_bounds;
    IndexTuple<NUM_ISOTOPES> upper_bounds;
    for (unsigned i = 0; i<NUM_ISOTOPES; ++i)
      if (max_corner_inclusive[i] < min_corner[i]) {
	lower_bounds.set(i, max_corner_inclusive[i]);
	upper_bounds.set(i, min_corner[i]);
      }
      else{
	lower_bounds.set(i, min_corner[i]);
	upper_bounds.set(i, max_corner_inclusive[i]);
    }

    for (unsigned long i = 0; i<NUM_ISOTOPES; ++i)
      lower_bounds.set(i, std::max(0,min_corner[i]));

    FixedSumTupleIterator<NUM_ISOTOPES>::apply(fixed_sum_tuple_iter, lower_bounds, upper_bounds, this->_power, lambda);
    return result;
  }
  
  // could maybe merge with the above function; since they do almost
  // the same thing, make this function templated template <typename
  // FUNCTION>, and accept a FUNCTION function parameter. That will be
  // your comparison operator. I think std::less<double> or something
  // like that would work to send, but also just a quick lambda
  // function: [](auto lhs, auto rhs){return lhs < rhs;} the merged
  // functions could be renamed
  // _extremal_probable_indiex_in_layer_product.  Also, since they're
  // computing the probability, might as well return std::pair<double,
  // IndexTuple>. Then the _log_multinomial_probability call can be
  // eliminated in the function below

  // Note: later, a divide and conquer may work for this. if it
  // accepts an element-space min corner and element-space max corner
  // (instead of a layer-space tile index), finding the tile closest
  // to the mode that has sum = _power could found by splitting the
  // tile on its longest axis, checking if any things in each half
  // haver _any_in_tile_that_sum_to_power. If any in half closer to
  // the mode have a sum == _power, then we will recurse on that
  // smaller half, otherwise recurse on the larger half (by getting to
  // this point, we should be guaranteed that the tile must contain
  // some value with sum == _power).
  IndexTuple<NUM_ISOTOPES> _least_probable_index_in_layer_product(const LayerProductIndex<NUM_ISOTOPES> & index) const {
    IndexTuple<NUM_ISOTOPES> min_corner = _min_corner_index(index.layer_indices);
    IndexTuple<NUM_ISOTOPES> max_corner_inclusive = _inclusive_max_corner_index(index.layer_indices);
    int fixed_sum_tuple_iter[NUM_ISOTOPES];
    IndexTuple<NUM_ISOTOPES> result;
    long l1_dist = std::numeric_limits<long>::min();
    auto lambda = [this, &result, &l1_dist](int*fixed_sum_tuple, unsigned dimension) {
      long tmp_l1_dist = 0;
      for (unsigned long i=0; i<dimension; ++i)
	tmp_l1_dist += abs(this->_mode[i] - fixed_sum_tuple[i]);
      
      if (tmp_l1_dist > l1_dist) {
	l1_dist = tmp_l1_dist;
	for (unsigned long i=0; i<dimension; ++i)
	  result.set(i, fixed_sum_tuple[i]);
      }
    };

    IndexTuple<NUM_ISOTOPES> lower_bounds;
    IndexTuple<NUM_ISOTOPES> upper_bounds;
    for (unsigned long i = 0; i<NUM_ISOTOPES; ++i)
      if (max_corner_inclusive[i] < min_corner[i]) {
	lower_bounds.set(i, max_corner_inclusive[i]);
	upper_bounds.set(i, min_corner[i]);
      }
      else {
	lower_bounds.set(i, min_corner[i]);
	upper_bounds.set(i, max_corner_inclusive[i]);
    }

    for (unsigned long i = 0; i<NUM_ISOTOPES; ++i) 
      lower_bounds.set(i, std::max(0,min_corner[i]));

    FixedSumTupleIterator<NUM_ISOTOPES>::apply(fixed_sum_tuple_iter, lower_bounds, upper_bounds, this->_power, lambda);
    return result;
  }

  std::pair<double, LayerProductIndex<NUM_ISOTOPES> > _create_layer_product(const LayerProductIndex<NUM_ISOTOPES> & index) const {
    IndexTuple<NUM_ISOTOPES> most_probable_index_in_layer_product;
    double result_log_prob;
    IndexTuple<NUM_ISOTOPES> result_index;
    if (index.is_max) {
      result_index = _least_probable_index_in_layer_product(index);
      result_log_prob = this->_log_multinomial_probability(result_index);
    }
    else {
      result_index = _most_probable_index_in_layer_product(index);
      result_log_prob = this->_log_multinomial_probability(result_index);
    }
    return std::make_pair(result_log_prob, index);
  }

  void _insert_layer_product_value(int*element_tup) {
    _values_considered.push_back(LogAbundanceAndMass{this->_log_multinomial_probability(element_tup), this->_compute_mass(element_tup)});
  }

  void _insert_layer_product_values_into_values_considered(const LayerProductIndex<NUM_ISOTOPES> & index) {
    IndexTuple<NUM_ISOTOPES> element_min_corner = _min_corner_index(index.layer_indices);
    IndexTuple<NUM_ISOTOPES> element_max_corner = _inclusive_max_corner_index(index.layer_indices);
    
    // At least one element is in bounds or they wouldn't have been
    // added; therefore, element_min_corner is in bounds. But
    // element_max_corner may not be. Note that min corner refers to
    // closer to the mode and max corner refers to further from the
    // mode (rather than smaller and larger indices).
    for (unsigned long i=0; i<NUM_ISOTOPES; ++i) {
      if (element_max_corner[i] < 0)
	element_max_corner.set(i, 0);

      if (element_max_corner[i] > int(this->_power))
	element_max_corner.set(i, int(this->_power));
    }
    // go through all IndexTuples between min_corner and max_corner that have the same sum
    
    // rearrange so that min_corner[i] <= max_corner[i]:
    for (unsigned long i=0; i<NUM_ISOTOPES; ++i) {
      int min_ind = element_min_corner[i], max_ind = element_max_corner[i];
      if (min_ind > max_ind) {
	element_min_corner.set(i, max_ind);
	element_max_corner.set(i, min_ind);
      }
    }

    int iteration_tuple[NUM_ISOTOPES];
    auto lambda = [this](auto tup_ptr, auto length) {
      this->_insert_layer_product_value(tup_ptr);
    };

    FixedSumTupleIterator<NUM_ISOTOPES>::apply(iteration_tuple, element_min_corner, element_max_corner, this->_power, lambda);
  }

  void _pop_next_layer_product() {
    #ifdef SAFE
    assert(_hull_heap.size()>0);
    #endif
    std::pop_heap(_hull_heap.begin(), _hull_heap.end());
    std::pair<double,LayerProductIndex<NUM_ISOTOPES> > next_layer_product = _hull_heap.back();
    _hull_heap.pop_back();

    if (next_layer_product.second.is_max) {
      // .num_values_contained will have been set below when this max layer product was inserted
      _num_max_corner_elements_popped += next_layer_product.second.num_values_contained;
    }
    else {
      LayerProductIndex<NUM_ISOTOPES> max_corner = next_layer_product.second;
      max_corner.is_max = true;

      unsigned long num_values_considered_before_min_layer_values = _values_considered.size();
      // Min corner must be popped before max corner; thus use min corner to add values. 
      _insert_layer_product_values_into_values_considered(next_layer_product.second);

      // Change in size of _values_considered is the size of this max layer product:
      max_corner.num_values_contained = _values_considered.size() - num_values_considered_before_min_layer_values;
      _insert_layer_product(max_corner);

      // Add new dependent min corners
      _insert_neighbors_in_bounds(next_layer_product.second.layer_indices);
    }
  }
public:
  BirthdayMultinomialLayerOrderedHeapGeneratorFixedSize(const LayerArithmetic*la, const double*log_probabilities, unsigned power, const double*masses, const LogFactorialCache & log_factorial_cache):
    MultinomialLayerOrderedHeapGeneratorFixedSize<NUM_ISOTOPES>(la, log_probabilities, power, masses, log_factorial_cache),
    _num_max_corner_elements_popped(0),
    _all_cumulative_selections_to_date(0),
    _hull_set(65536) // reserve space
  {
    IndexTuple<NUM_ISOTOPES> mode_zero_indexed;
    _insert_layer_product( LayerProductIndex<NUM_ISOTOPES>{mode_zero_indexed, false} );
  }

  void compute_next_layer_if_available() override {
    if (this->are_more_layers_available()) {
      unsigned long new_layer_size = this->layer_size(this->_number_of_layers_generated);
      LogAbundanceAndMass*new_layer = _select(new_layer_size);
      this->add_layer(new_layer, new_layer_size);
    }
  }
};

#endif
