#ifndef _ISOTOPECALCULATOR_HPP
#define _ISOTOPECALCULATOR_HPP

#include <unordered_map>
#include "ElementMultinomialLayerOrderedHeapGenerator.hpp"
#include "APlusBLayerOrderedHeapGenerator.hpp"
#include "ArrayLayerOrderedHeapGenerator.hpp"
#include "element_to_log_abundance_and_masses.hpp"
#include <cstring>

double log_add(double log_a, double log_b) {
  if (log_a < log_b)
    std::swap(log_a, log_b);
  return log_a + log1p(exp(log_b-log_a));
}

inline double faster_exp(const double val) {
  union {
    double d;
    int64_t x;
  } u;
  u.x = (1072693248L - 90253)<<32;
  u.x += (1512775L<<32) * val;
  return u.d;
}

constexpr unsigned NUM_TO_SORT = 512;

class IsotopeCalculator {
private:
  LogFactorialCache*_log_factorial_cache;
  std::vector<std::pair<std::string, unsigned long> > _elements_to_count;
  LayerOrderedHeapGenerator<LogAbundanceAndMass>* _root;
  
  // Used by destructor:
  PrimitiveVector<LayerOrderedHeapGenerator<LogAbundanceAndMass>*> _tree_nodes;
  
  LayerArithmetic *_la;
  
  void _print_read_error() {
    printn("ERROR: Bad compound in put; there must be a number after each element (if only one oxygen input O1), input like H2O1 for water");
    exit(1);
  }
  
  void _parse_molecule(const std::string &molecule) {
    auto element_start = 0U;
    auto element_len = 0U;    
    auto count_start = 0U;
    auto count_len = 0U;
    unsigned long i=0;
    while (i < molecule.size()) {
      if (isalpha(molecule[i]) != 0)
        element_start = i;
      
      while(isalpha(molecule[i]) != 0)
        ++i;
      
      element_len = i - element_start;
      std::string element = molecule.substr(element_start, element_len);
      
      if (isdigit(molecule[i]) != 0)
        count_start = i;
      else
        _print_read_error();
      
      
      while(isdigit(molecule[i]) != 0)
        ++i;
      count_len = i - count_start;
      if (count_len == 0)
        _print_read_error();
      
      
      long count = read<long>(molecule.substr(count_start, count_len).c_str());
      _elements_to_count.push_back(std::make_pair(element, count));
    }
  }
  
  void _build_log_factorial_cache() {
    unsigned long largest_factorial = 0;
    for (const auto & element_and_count : _elements_to_count)
      largest_factorial = std::max(largest_factorial, element_and_count.second);
    largest_factorial += ElementMultinomialLayerOrderedHeapGenerator::MAX_NUMBER_ISOTOPES_SUPPORTED;
    _log_factorial_cache = new LogFactorialCache(largest_factorial);
  }
  
  void _build_tree() {
    PrimitiveVector<LayerOrderedHeapGenerator<LogAbundanceAndMass>* > current_layer;
    for (const auto & element_and_count : _elements_to_count) {
      ElementMultinomialLayerOrderedHeapGenerator*leaf = new ElementMultinomialLayerOrderedHeapGenerator(element_and_count.first, element_and_count.second, _la, *_log_factorial_cache);
      current_layer.push_back(leaf);
      
      _tree_nodes.push_back(leaf);
    }
    
    while (current_layer.size() > 1) {
      PrimitiveVector<LayerOrderedHeapGenerator<LogAbundanceAndMass>* > next_layer;
      for (unsigned long i=0; i < (current_layer.size()>>1); ++i) {
        auto internal_node = new APlusBLayerOrderedHeapGenerator<LogAbundanceAndMass>(current_layer[2*i], current_layer[2*i + 1], _la);
        next_layer.push_back(internal_node);
        _tree_nodes.push_back(internal_node);
      }
      
      if (current_layer.size()%2 == 1)
        next_layer.push_back(current_layer[current_layer.size() - 1]);
      current_layer = next_layer;
    }
    _root = current_layer[0];
  }
  
  
  void _recursively_lohify_to_find_needed_from_last_layer(LogAbundanceAndMass* layer_begin, LogAbundanceAndMass* layer_end, double total_abundance, const double p, unsigned long & offset, std::function<bool(const LogAbundanceAndMass&,const LogAbundanceAndMass&)> compare = [](const auto & lhs, const auto & rhs){return lhs<rhs;}) {
    const unsigned long n = layer_end - layer_begin;
    if (n <= NUM_TO_SORT) {
      std::sort(layer_begin, layer_end);
      LogAbundanceAndMass* ptr = layer_begin;
      while (total_abundance < p && ptr < layer_end) {
        total_abundance += faster_exp(ptr[0].log_abundance);
        ++ptr;
        ++offset;
      }
      printn("total abundance, p", total_abundance, p);
      return;      
    }
    
    // If dont return, lohify then recurse
    const unsigned long*pivot_ptr;
    for (pivot_ptr=_la->begin(); pivot_ptr!=_la->end(); ++pivot_ptr) {
      if (*pivot_ptr >= n)
        break;
    }
    
    // pivot_ptr is one past the final relevant pivot:
    random_lohify(layer_begin, layer_end, _la->begin(), pivot_ptr, compare);
    
    double abundance_of_last_layer = 0;
    unsigned long layer = 0;    
    
    LogAbundanceAndMass* start = layer_begin + _la->get_flat_layer_start_index(layer);
    LogAbundanceAndMass* end = layer_begin + std::min(_la->get_flat_layer_end_index(layer), (long unsigned int)(layer_end-layer_begin));
    
    abundance_of_last_layer = _calculate_total_abundance_of_layer(start, end);
    ++layer;
    
    while(abundance_of_last_layer + total_abundance < p) {
      total_abundance += abundance_of_last_layer;
      offset += end-start;
      start = layer_begin + _la->get_flat_layer_start_index(layer);
      end = layer_begin + std::min(_la->get_flat_layer_end_index(layer), (long unsigned int)(layer_end-layer_begin));
      abundance_of_last_layer = _calculate_total_abundance_of_layer(start, end);
      ++layer;
    }
    
    _recursively_lohify_to_find_needed_from_last_layer(start, end, total_abundance, p, offset);
  }
  
  double _calculate_total_abundance_of_layer(LogAbundanceAndMass* layer_begin, LogAbundanceAndMass* layer_end) {
    double total_abundance = 0;
    LogAbundanceAndMass* i = layer_begin;
    while (i < layer_end) {
      total_abundance += faster_exp(i[0].log_abundance);
      ++i;
    }
    return total_abundance;
  }
  
  unsigned long _retrieve_all_but_last_layer_get_number_retrieved(LogAbundanceAndMass*__restrict result, unsigned long k, unsigned long upper_layer_limit) {
    unsigned long result_index = 0;
    unsigned long layer;
    
    for (layer = 0; layer < upper_layer_limit; ++layer) {
      LogAbundanceAndMass*start = _root->layer_begin(layer);
      LogAbundanceAndMass*end = _root->layer_end(layer);
      
      unsigned long length = (end-start);
      memcpy(result+result_index, start, length*sizeof(LogAbundanceAndMass));
      result_index += length;
    }
    
    return result_index;
  }
  
  void _retrieve_layers_from_root(LogAbundanceAndMass*__restrict result, unsigned long k) {
    unsigned long layer  = _root->n_layers_generated()-1;
    unsigned long result_index = _retrieve_all_but_last_layer_get_number_retrieved(result, k, layer);
    
    LogAbundanceAndMass*start = _root->layer_begin(layer);
    LogAbundanceAndMass*end = _root->layer_end(layer);
    
    unsigned long num_elements_used_from_last_layer = k - result_index;
    std::nth_element(start, start+num_elements_used_from_last_layer-1, end);
    memcpy(result+result_index, start, num_elements_used_from_last_layer*sizeof(LogAbundanceAndMass));
  }
  
  void _retrieve_layers_from_root_without_partition(LogAbundanceAndMass*__restrict result, unsigned long k) {
    unsigned long layer = _root->n_layers_generated() - 1;
    unsigned long result_index = _retrieve_all_but_last_layer_get_number_retrieved(result, k, layer);
    
    unsigned long num_elements_used_from_last_layer = k - result_index;
    LogAbundanceAndMass*start = _root->layer_begin(layer);
    memcpy(result+result_index, start, num_elements_used_from_last_layer*sizeof(LogAbundanceAndMass));
  }
  
public:
  IsotopeCalculator(std::string &molecule, LayerArithmetic* la):
    _la(la)
  {
    _parse_molecule(molecule);
    _build_log_factorial_cache();
    _build_tree();
  }
  
  ~IsotopeCalculator() {
    for (auto node_ptr : _tree_nodes)
      delete node_ptr;
    delete _log_factorial_cache;
  }
  
  
  LogAbundanceAndMass* select(unsigned long & k) {
    if (k > _root->n_elements_possible()) {
      std::cerr << "Warning: requested " << k << " values, but only " << _root->n_elements_possible() << " are possible" << std::endl;
      k = _root->n_elements_possible();
    }
    
    while (_root->n_elements_generated() < k)
      _root->compute_next_layer_if_available();
    
    LogAbundanceAndMass* result = new LogAbundanceAndMass[k];
    _retrieve_layers_from_root(result, k);
    return result;
  }
  
  std::pair<LogAbundanceAndMass*, unsigned long> select_by_p(double p) {
    if (p <= 0 || p > 1.0) {
      std::cerr << "ERROR: requested p = " << p << " which is outside (0,1]" << std::endl;
      exit(1);
    }
    
    double not_logged_p = p;
    p = log(p);
    
    double total_abundance_before_last_layer = 0;
    double abundance_of_last_layer = 0;
    unsigned long number_of_layers_generated = 0;
    unsigned long num_elements = 0;
    
    _root->compute_next_layer_if_available();
    ++number_of_layers_generated;
    abundance_of_last_layer = _calculate_total_abundance_of_layer(_root->layer_begin(number_of_layers_generated-1), _root->layer_end(number_of_layers_generated-1));
    num_elements += _root->layer_end(number_of_layers_generated-1) - _root->layer_begin(number_of_layers_generated-1);
    
    while(abundance_of_last_layer+total_abundance_before_last_layer < not_logged_p && _root->are_more_layers_available()) {
      total_abundance_before_last_layer += abundance_of_last_layer;
      _root->compute_next_layer_if_available();
      ++number_of_layers_generated;
      num_elements += _root->layer_end(number_of_layers_generated-1) - _root->layer_begin(number_of_layers_generated-1);
      abundance_of_last_layer = _calculate_total_abundance_of_layer(_root->layer_begin(number_of_layers_generated-1), _root->layer_end(number_of_layers_generated-1));
    }
    num_elements -= _root->layer_end(number_of_layers_generated-1) - _root->layer_begin(number_of_layers_generated-1);
    
    // Recurse on last layer to get minimum set of peaks which together exceed p
    unsigned long offset = 0;
    
    _recursively_lohify_to_find_needed_from_last_layer(_root->layer_begin(number_of_layers_generated-1), _root->layer_end(number_of_layers_generated-1), total_abundance_before_last_layer, not_logged_p, offset);
    
    const unsigned long amount_in_last_layer = _root->layer_end(number_of_layers_generated-1) - _root->layer_begin(number_of_layers_generated-1);
    const unsigned long k = _root->n_elements_generated() - (amount_in_last_layer - offset);
    
    unsigned long size_before_last_layer = 0;
    for (unsigned long layer = 0; layer+1 < _root->n_layers_generated(); ++layer)
      size_before_last_layer += _root->layer_end(layer) - _root->layer_begin(layer);
    
    LogAbundanceAndMass* result = new LogAbundanceAndMass[k];
    
    _retrieve_layers_from_root_without_partition(result, k);
    printn("TOTAL PEAKS FOUND:", k);
    return {result,k};
  }
  
};
#endif
