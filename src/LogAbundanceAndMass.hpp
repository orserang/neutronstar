#ifndef _LOGABUNDANCEANDMASS_HPP
#define _LOGABUNDANCEANDMASS_HPP

#include <math.h>
#include <iostream>

struct LogAbundanceAndMass {
  double log_abundance, mass;

  const LogAbundanceAndMass & operator +=(const LogAbundanceAndMass & rhs) {
    log_abundance += rhs.log_abundance;
    mass += rhs.mass;
    return *this;
  }
};

std::ostream & operator<<(std::ostream & os, LogAbundanceAndMass laam) {
  os << laam.log_abundance << "@m=" << laam.mass;
  return os;
}

// We rank on abundance alone; mass is there for the final answer only.
// Operators are reversed to perform max selection.

// Note: inverted so that min-selections become max-selections:
bool operator <(const LogAbundanceAndMass & lhs, const LogAbundanceAndMass & rhs) {
  return lhs.log_abundance > rhs.log_abundance;
}
bool operator <=(const LogAbundanceAndMass & lhs, const LogAbundanceAndMass & rhs) {
  return lhs.log_abundance >= rhs.log_abundance;
}
bool operator >(const LogAbundanceAndMass & lhs, const LogAbundanceAndMass & rhs) {
  return lhs.log_abundance < rhs.log_abundance;
}
bool operator >=(const LogAbundanceAndMass & lhs, const LogAbundanceAndMass & rhs) {
  return lhs.log_abundance <= rhs.log_abundance;
}

LogAbundanceAndMass operator +(LogAbundanceAndMass lhs, const LogAbundanceAndMass & rhs) {
  lhs += rhs;
  return lhs;
}

#endif
