#ifndef _IO_HPP
#define _IO_HPP

#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>
#include <tuple>
#include <assert.h>

template <typename Iter>
std::string str(const Iter & st, const Iter & en) {
  std::ostringstream ost;
  std::for_each(st, en, [&ost](const auto & v){ost << v << " ";});
  return ost.str();
}

template <typename Iterable>
std::string str(const Iterable & container) {
  return str(container.begin(), container.end());
}

// Manual std::operator<< is invoked to prevent circularity: variadic << operator below could match std::string. 

template <template <typename...> typename Iterable, typename ...T>
std::ostream & operator <<(std::ostream & os, const Iterable<T...> & container) {
  std::operator<<(os, str(container));
  return os;
}

int indent=0;

// Similar to manual invocation of std::operator<< above: use
// print_single_with_space_no_indent to distinguish printing
// std::string vs. other data structures.

template <typename V>
void print_single_with_space_no_indent(std::ostream & os, const V & v) {
  os << v << " ";
}

void print_single_with_space_no_indent(std::ostream & os, const std::string & v) {
  std::operator<<(os, v+" ");
}

void print_single_with_space_no_indent(bool v) {
  if (v)
    print_single_with_space_no_indent("true");
  else
    print_single_with_space_no_indent("false");
}

// To suppress "unused variable" warning
void do_nothing(int x[]) {
}

template <typename ...V>
void print_no_indent(V... v, std::ostream & os = std::cout) {
  int garbage[] = { (print_single_with_space_no_indent(os, v), 0)... };
  do_nothing(garbage);
}

template <typename ...V>
void print(V... v, std::ostream & os = std::cout) {
  int local_indent=indent;
  while(local_indent>0) {--local_indent; os << ' ';}
  print_no_indent<V...>(v..., os);
}

template<int ...>
struct seq { };

template<int N, int ...S>
struct gens : gens<N-1, N-1, S...> { };

template<int ...S>
struct gens<0, S...> {
  typedef seq<S...> type;
};

template <int ...S, typename ...V>
void print_tup(std::ostream & os, const std::tuple<V...> & tup, seq<S...> s) {
  os << "( ";
  print_no_indent<V...>(std::get<S>(tup)..., os);
  os << ")";
}

template <typename ...V>
std::ostream & operator <<(std::ostream & os, const std::tuple<V...> & tup) {
  print_tup(os, tup, typename gens<sizeof...(V)>::type());
  return os;
}

template <typename V1, typename V2>
std::ostream & operator <<(std::ostream & os, const std::pair<V1, V2> & tup) {
  os << "( ";
  os << tup.first;
  os << " ";
  os << tup.second;
  os << " )";
  return os;
}

template <typename ...V>
void printn(V... v) {
  print<V...>(v...);
  std::cout << std::endl;
}

template <typename T>
T read(const char*arg) {
  std::istringstream ist(arg);
  T result;
  assert(ist >> result);
  return result;
}

#endif
