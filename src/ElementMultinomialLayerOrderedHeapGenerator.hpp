#ifndef _ELEMENTMULTINOMIALLAYERORDEREDHEAPGENERATOR_HPP
#define _ELEMENTMULTINOMIALLAYERORDEREDHEAPGENERATOR_HPP

#include "SortMultinomialLayerOrderedHeapGenerator.hpp"
#include "element_to_log_abundance_and_masses.hpp"
#include "TemplateSearch.hpp"

template <unsigned long NUM_ISOTOPES>
struct SortMultinomialLayerOrderedHeapGeneratorFactory {
  static void apply(MultinomialLayerOrderedHeapGenerator*&mlohg, LayerArithmetic*la, const double*log_probabilities, unsigned power, const double*masses, const LogFactorialCache & log_factorial_cache) {
    mlohg = new SortMultinomialLayerOrderedHeapGeneratorFixedSize<NUM_ISOTOPES>(la, log_probabilities, power, masses, log_factorial_cache);
  }
};

class ElementMultinomialLayerOrderedHeapGenerator : public LayerOrderedHeapGenerator<LogAbundanceAndMass> {
protected:
  MultinomialLayerOrderedHeapGenerator*_mlohg;

  unsigned long _num_isotopes;
public:
  static constexpr unsigned long MAX_NUMBER_ISOTOPES_SUPPORTED = 12;

  ElementMultinomialLayerOrderedHeapGenerator(const std::string & element, unsigned long num_copies, LayerArithmetic*la, const LogFactorialCache & log_factorial_cache):
    LayerOrderedHeapGenerator<LogAbundanceAndMass>(la)
  {
    if (element_to_log_abundance_and_masses.find(element) == element_to_log_abundance_and_masses.end()){
      printn("ERROR: element \"", element, "\" not found in element_to_log_abundance_and_masses. Make sure to have a number following the element abreviation (e.g. H2SO4 should be input as H2S1O4)");
      exit(1);      
    }
      
    const std::pair<std::vector<double>, std::vector<double> > & log_abundances_and_masses = element_to_log_abundance_and_masses.find(element)->second;

    _num_isotopes = log_abundances_and_masses.first.size();
    #ifdef SAFE
    assert(_num_isotopes <= MAX_NUMBER_ISOTOPES_SUPPORTED);
    #endif

    const double*log_probabilities = &log_abundances_and_masses.first[0];
    const double*masses = &log_abundances_and_masses.second[0];

    LinearTemplateSearch<1,MAX_NUMBER_ISOTOPES_SUPPORTED, SortMultinomialLayerOrderedHeapGeneratorFactory>::apply(_num_isotopes, _mlohg, _la, log_probabilities, num_copies, masses, log_factorial_cache);

    this->_number_of_elements_possible = _mlohg->n_elements_possible();
  }

  ~ElementMultinomialLayerOrderedHeapGenerator() {
    delete _mlohg;
  }
  
  LogAbundanceAndMass*layer_begin(unsigned long layer_i) const override {
    return _mlohg->layer_begin(layer_i);
  }

  LogAbundanceAndMass*layer_end(unsigned long layer_i) const override {
    return _mlohg->layer_end(layer_i);
  }

  void compute_next_layer_if_available() override {
    _mlohg->compute_next_layer_if_available();
    ++this->_number_of_layers_generated;
    this->_number_of_elements_generated = _mlohg->n_elements_generated();
  }
};

#endif
