#ifndef _FIXEDSUMTUPLEITERATOR_HPP
#define _FIXEDSUMTUPLEITERATOR_HPP

#include "TemplateSearch.hpp"

// See TRIOT paper, Heyl & Serang
template <unsigned TUPLE_LENGTH, unsigned TUPLE_INDEX>
struct FixedSumTupleIteratorHelper {
  template <typename FUNCTION>
  static void apply(int*__restrict tup, const int*__restrict tup_start, const int*__restrict tup_end_inclusive, int sum, FUNCTION function) {
    for (int i=tup_start[TUPLE_INDEX]; i<=std::min(sum, tup_end_inclusive[TUPLE_INDEX]); ++i) {
      tup[TUPLE_INDEX] = i;
      FixedSumTupleIteratorHelper<TUPLE_LENGTH, TUPLE_INDEX-1>::apply(tup, tup_start, tup_end_inclusive, sum-i, function);
    }
  }
};

template <unsigned TUPLE_LENGTH>
struct FixedSumTupleIteratorHelper<TUPLE_LENGTH, 0u> {
  template <typename FUNCTION>
  static void apply(int*__restrict tup, const int*__restrict tup_start, const int*__restrict tup_end_inclusive, int sum, FUNCTION function) {
    // Compiler may be able to eliminate this if statement and move it into the loop bounds of the <1u> base case:
    if (sum >= tup_start[0] && sum <= tup_end_inclusive[0]) {
      tup[0] = sum;
      function(tup, TUPLE_LENGTH);
    }
  }
};

template <unsigned int TUPLE_LENGTH>
struct FixedSumTupleIterator {
  template <typename FUNCTION>
  static void apply(int*__restrict tup, const int*__restrict tup_start, const int*__restrict tup_end_inclusive, int sum, FUNCTION function) {
    FixedSumTupleIteratorHelper<TUPLE_LENGTH, TUPLE_LENGTH-1>::apply(tup, tup_start, tup_end_inclusive, sum, function);
  }
};

#endif
