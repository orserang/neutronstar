## NEUTRONSTAR

NeutronStar is the worlds fastest isotope calculator!  NeutronStar utilizes the
layer-ordered heap data structure and optimal pair-wise selection on
the Cartesian product to calculate millions of isotopologues of a
given compound in less than a second.

Written in C++17, NeutronStar is available to use subject to the MIT
license discussed in license.txt file.

An arxiv version of this paper is available at
https://arxiv.org/pdf/2004.07444.pdf - the paper is currently
submitted and publishment is pending.

## Compile
Create executable using "make" in the /src/ directory

Or just run "g++ -g -std=c++17 -O3 -march=native -mtune=native -Wall neutron_star.cpp -o neutronstar" in the /src/  directory.  C++17 is required!

## USAGE
Executable will be called neutronstar.

Usage:  
./neutronstar \<molecule (e.g. water is H2O1)\> \<-k or -p\> \<k or p\> \<alpha\> [merge_identical_masses] [sort_by_abundance]

**PARAMETERS:**

Chemical formula is entered using the pattern \<first element symbol\>\<number of first element\>\<second element symbol\>\<number of second element\>... where each element MUST have a number following it (e.g. water is H2O1 not just H2O).

-k or -p: flag declaring what the next parameter will be. -k means
 you want the top k most abundant peaks. -p means you want the
 smallest set of peaks which together have a total abundance of at least p.

k (if -k flag is used) determines number of peaks to fetch

p (if -p flag is used) smallest set of peaks which together have a total abundance of at least p

alpha determines the size of the layers in the layer ordered heaps,
alpha=1.0 means to sort everything so alpha>1.0 will increase. In
practice, for -k alpha = 1.05 is good, for -p alpha = 1.005 is
good. Alpha=1.01 is good for both -k and -p, though not as good as
the previously listed alpha values.

merge_identical_masses flag will merge peaks if their masses are the
same (up to machine precision).

sort_by_abundance flag sorts the output by abundance in descending
order; this will hurt performance but it only sorts after the output
has been calculated.

## EXAMPLES

**EXAMPLE USES**

Calculate isotopes of Bovine insulin (C254H377N65O75S6), finding top 1,000 peaks with alpha=1.05, merges identical masses and sorts the output by abundance:  
./neutronstar C254H377N65O75S6 -k 1000 1.05

Calculate isotopes of Bovine insulin, finding the minimal set of peaks whose combined abundance is at least 0.9 with alpha=1.05, merges identical masses and sorts the output by abundance:  
./neutronstar C254H377N65O75S6 -k 1000 1.05	 

To merge identical masses:  
./neutronstar C254H377N65O75S6 -k 1000 1.05 merge_identical_masses

To merge identical masses and report peaks in sorted order:  
./neutronstar C254H377N65O75S6 -k 1000 1.05 merge_identical_masses sort_by_abundance


**EXAMPLE MOLECULES** 

While NeutronStar shines with both organic and inorganic molecules,
organic elements typically have very few isotopes. To really test
NeutronStar, through some compounds whose elements have a high number
if isotopes at it! Elements link Xe, Sn, Dy, Pd are all great choices.

Palladium alloy Pgc:  
./neutronstar Au2Ca10Ga10Pd76 -k 1000 1.05

Ostalloy:  
./neutronstar Bi50Cd12Pb25Sn12 -k 1000 1.05

Bovine insulin:  
./neutronstar C254H377N65O75S6 -k 1000 1.05    

Hemoglobin: 	  
./neutronstar C2932H4723N828O840S8Fe4 -k 1000 1.05	 

Titin protein (largest protein in humans):  
./neutronstar C169719H270466N45688O52238S911 -k 1000 1.05	 

human dynein heavy chain:  
./neutronstar C23832H37816N6528O7031S170 -k 1000 1.05	 

BRCA_2 human (gene commonly linked to breast cancer in humans):      
./neutronstar C16802H26738N4640O5411S121 -k 1000 1.05	 
